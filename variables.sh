#!/bin/bash

export ABBREV='rvm-rails-tutorial6'
export OWNER='jhsu802701'
export SUITE='bullseye'
export DISTRO='debian'
export DOCKER_IMAGE="registry.gitlab.com/$OWNER/docker-$DISTRO-$SUITE-$ABBREV"
export DOCKER_CONTAINER="container-$DISTRO-$SUITE-$ABBREV"
