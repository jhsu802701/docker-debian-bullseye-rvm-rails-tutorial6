[![pipeline status](https://gitlab.com/jhsu802701/docker-debian-bullseye-rvm-rails-tutorial-6/badges/main/pipeline.svg)](https://gitlab.com/jhsu802701/docker-debian-bullseye-rvm-rails-tutorial-6/-/commits/main)

# Docker Debian Bullseye - RVM - Rails - Tutorial 6

I am using this RVM Rails Tutorial 6 Debian Bullseye Docker image for going through [Rails Tutorial 6](https://www.railstutorial.org/).

## Name of This Docker Image
[registry.gitlab.com/jhsu802701/docker-debian-bullseye-rvm-rails-tutorial6](https://gitlab.com/jhsu802701/docker-debian-bullseye-rvm-rails-tutorial6/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-bullseye-min-rvm](https://gitlab.com/rubyonracetracks/docker-debian-bullseye-min-rvm/container_registry)

## What's Added
* The latest versions of the rails, pg, nokogiri, and ffi gems
* The versions of the above gems used in the Rails Tutorial App.
* Bundler
* The mailcatcher gem

## What's the Point?
* Rails Tutorial isn't dockerized.  It's based on the assumption that you're doing your Ruby on Rails development in your host environment.
* I don't rely on my host environment for Ruby on Rails development.  Not being able to reset it willy-nilly is a deal breaker.
* Thus, I rely on a Bash window connected to a Docker container as my development environment.  It's the best of both worlds!

## More Information
General information common to all Docker Debian build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-common/blob/main/FAQ.md).
